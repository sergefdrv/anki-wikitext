# Wikitext add-on for Anki

This add-on for [Anki](http://ankisrs.net) enables use of
[Creole](http://wikipedia.org/wiki/Creole_%28markup%29) markup language in
Anki's note editor. This add-on is only available for desktop Anki. But once you
have formatted some text in notes with it, you can later review your cards with
any Anki implementation.

## Usage

Use "W" button in Anki's note editor or press "Ctrl+Shift+W". A new window is
opened to edit a text in Creole wiki markup syntax. If you have some text
selected, only that part will be edited. Once you done, just close the window.

## Installation

### From AnkiWeb add-on page

Please visit the [add-on's page](https://ankiweb.net/shared/info/960358571) for
installation instructions.

### From source code

**Prerequisites.** Installation from source code requires a *Unix-like shell* and
*Python 2.x* interpreter.

Any version can be downloaded from a [project
repository](https://gitlab.com/sergefdrv/anki-wikitext). Use `make.sh` to
install the add-on.

#### Normal install

Use this method for normal installation. Execute `./make.sh install DEST` to
install the add-on into directory *DEST*.  Add-ons should normally be installed
in `addon` directory under user's Anki directory.

#### Symlink install

Use this method to set up a development environment so that your changes to the
source code will be effective in each Anki run. Execute `./make.sh symlinks
DEST` to install the add-on into directory *DEST*.  Add-ons should normally be
installed in `addon` directory under user's Anki directory.

#### Package

Use this method for preparing a zip-package to distribute the add-on. Execute
`./make.sh package DEST` to make a package *DEST* (`wikitext.zip` by default).
This package can be extracted to `addon` directory under user's Anki directory
in order to install the add-on.

## Reporting bugs

Please report any issues by submitting [this
form](https://gitlab.com/sergefdrv/anki-wikitext/issues/new) or by email
<sergefdrv@gmail.com>.

## Authors

Please see [AUTHORS.txt](AUTHORS.txt) file.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

## History

 * v0.1.0: first version

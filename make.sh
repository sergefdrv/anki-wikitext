#!/bin/sh

# Wikitext add-on for Anki

# Copyright (c) 2015 Sergey Fedorov <serge.fdrv@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eu

usage() {
    echo "Usage: `basename $0` install|package|symlinks|uninstall [DEST]\n"
    echo "  install    install to DEST"
    echo "  package    make package and save it as DEST (\`wikitext.zip' by default)"
    echo "  symlinks   setup symlinks in DEST"
    echo "  uninstall  uninstall from DEST"
}

abort() {
    usage >&2
    exit 1
}

absolute_path() {
    case "$1" in
        /*) echo "$1";;
        *)  echo "$PWD/$1";;
    esac
}

install_python_creole() {
    dest=$1
    install -vd "$dest/creole"
    install -vt "$dest/creole" \
        "LICENSE" \
        "AUTHORS"
    python "setup.py" --no-user-cfg install_lib --install-dir="$dest"
}

install_addon() {
    dest=$1
    install -vt "$dest" \
        "wikitext.py"
    install -vd "$dest/wikitext"
    install -vt "$dest/wikitext" \
        "LICENSE.txt" \
        "AUTHORS.txt" \
        "README.md" \
        "VERSION" \
        "wikitext/__init__.py" \
        "wikitext/wikitext.py"
    python -m compileall "$dest/wikitext.py" "$dest/wikitext"

    echo "Installing \`python-creole'..."
    creole_dir="$PWD/wikitext/python-creole"
    orig=$PWD
    echo "entering \`$creole_dir'"
    cd "$creole_dir"
    install_python_creole "$dest/wikitext"
    echo "leaving back to \`$orig'"
    cd "$orig"
}

do_install() {
    dest=$1
    if [ -z "$dest" ]; then
        echo "Error: destination folder is required\n"
        abort
    fi
    dest=`absolute_path "$dest"`
    if [ ! -e "$dest" ]; then
        install -vd "$dest"
    elif [ ! -d "$dest" ]; then
        echo "Error: destination is not a folder\n"
        abort
    fi
    echo "Installing to \`$dest'..."
    install_addon "$dest"
    echo "Installed successfully to \`$dest'"
}

do_package() {
    pack=${1:-"wikitext.zip"}
    pack=`absolute_path "$pack"`
    echo "Making package \`$pack'"
    dist="$PWD/dist"
    install -vd "$dist"
    install_addon "$dist"
    echo "making zip archive \`$pack'"
    cd "$dist"
    zip -r "$pack" *
    cd ..
    echo "Package \`$pack' successfully made'"
}

do_symlinks() {
    dest=$1
    if [ -z "$dest" ]; then
        echo "Error: destination folder is required\n"
        abort
    fi
    dest=`absolute_path "$dest"`
    if [ ! -e "$dest" ]; then
        install -vd "$dest"
    elif [ ! -d "$dest" ]; then
        echo "Error: destination is not a folder\n"
        abort
    fi
    echo "Installing symlinks to \`$dest'..."
    ln -vfs "python-creole/creole" "wikitext/"
    ln -vfst "$dest" \
        "$PWD/wikitext.py" \
        "$PWD/wikitext"
    echo "Symlinks installed successfully to \`$dest'"
}

do_uninstall() {
    dest=$1
    if [ -z "$dest" ]; then
        echo "Error: destination folder is required\n"
        abort
    fi
    dest=`absolute_path "$dest"`
    if [ ! -d "$dest" ]; then
        echo "Error: destination is not a folder\n"
        abort
    fi
    echo "Uninstalling from \`$dest'..."
    rm -vrf "$dest/wikitext.py" "$dest/wikitext.pyc" "$dest/wikitext"
    echo "Uninstalled successfully from \`$dest'"
}

[ $# -ge 1 ] || abort

case $1 in
    install)   do_install   "$2";;
    package)   do_package   "${2:-}";;
    symlinks)  do_symlinks  "$2";;
    uninstall) do_uninstall "$2";;
    *) abort;;
esac

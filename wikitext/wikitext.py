# -*- coding: utf-8 -*-

# Wikitext add-on for Anki

# Copyright (c) 2015 Sergey Fedorov <serge.fdrv@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Wikitext Anki add-on main implementation
"""

import sys, os
import re
from anki.hooks import addHook
from aqt.editor import Editor
from aqt.utils import openLink
from anki.utils import json
from PyQt4.QtCore import *
from PyQt4.QtGui import *
# Workaround for Anki bug: Encode system path elements with the filesystem encoding
sys.path = map(lambda(e): e.encode(sys.getfilesystemencoding()), sys.path)
# Add the add-on directory for module searching
sys.path.insert(0, os.path.dirname(__file__))
from creole import html2creole
from creole.shared.unknown_tags import use_html_macro
from creole.shared import example_macros
from creole.creole2html.parser import CreoleParser
from creole.creole2html.emitter import HtmlEmitter

class WikitextHtmlEmitter(HtmlEmitter):
    def paragraph_emit(self, node):
        return '<div>%s</div>' % self.emit_children(node)

def openHelp():
    openLink("http://en.wikipedia.org/wiki/Creole_(markup)")

def onWikitextEdit(self):
    self.saveNow()
    dialog = QDialog(self.widget)
    dialog.resize(400, 300)
    dialog.verticalLayout = QVBoxLayout(dialog)
    dialog.textEdit = QTextEdit(dialog)
    dialog.textEdit.setAcceptRichText(False)
    dialog.verticalLayout.addWidget(dialog.textEdit)
    dialog.buttonBox = QDialogButtonBox(dialog)
    dialog.buttonBox.setOrientation(Qt.Horizontal)
    dialog.buttonBox.setStandardButtons(QDialogButtonBox.Close |
                                        QDialogButtonBox.Help)
    dialog.verticalLayout.addWidget(dialog.buttonBox)
    dialog.setWindowTitle(_("Wikitext Editor"))
    QObject.connect(dialog.buttonBox, SIGNAL("rejected()"), dialog.reject)
    QObject.connect(dialog.buttonBox, SIGNAL("helpRequested()"), openHelp)
    html = self.web.selectedHtml()
    if (not html):
        self.web.eval("document.execCommand('selectAll', false, null);")
        html = self.web.selectedHtml()
    html = re.sub(r'({{[^{}]*}})', r'~\1', html)
    html = re.sub(r'(\[[^][]*\])', r'~\1', html)
    wikitext = html2creole(html, unknown_emit = use_html_macro)
    dialog.textEdit.setPlainText(wikitext)
    dialog.textEdit.moveCursor(QTextCursor.End)
    dialog.exec_()
    wikitext = dialog.textEdit.toPlainText()
    document = CreoleParser(wikitext).parse()
    kwargs = { "macros": {"html": example_macros.html}}
    html = WikitextHtmlEmitter(document, **kwargs).emit()
    self.web.eval("document.execCommand('insertHTML', false, %s);" %
            json.dumps(html))
    self.web.setFocus()
    self.web.eval("focusField(%d);" % self.currentField)

def onEditorButtonsSetup(editor):
    editor._addButton("wikitext", editor.onWikitextEdit, _("Ctrl+Shift+W"),
                      _("Edit as Wikitext (Ctrl+Shift+W)"), text="W")

def init():
    Editor.onWikitextEdit = onWikitextEdit
    addHook("setupEditorButtons", onEditorButtonsSetup)
